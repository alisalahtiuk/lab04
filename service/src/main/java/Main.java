import org.json.JSONObject;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        Book newBook = new Book("The Littles", "John Peterson", "9780590462259", false, "");
        User newUser = new User("Dasha", "Shum", 19);
        JSONObject takeBook = LibOperations.takeBook(newUser.firstName, newBook);
        System.out.println(takeBook);

        //LibInfo.writeLibInfo(takeBook);
    }

}

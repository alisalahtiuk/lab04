import org.json.JSONException;
import org.json.JSONObject;

public class LibOperations {


    public static JSONObject takeBook(String firstName, Book anyBook){
        JSONObject newObj = new JSONObject();
        try {
            newObj.put("name", anyBook.name);
            newObj.put("author", anyBook.author);
            newObj.put("isbn", anyBook.isbn);
            newObj.put("taken", true);
            newObj.put("taken_by", firstName);
        } catch (JSONException e){
            e.printStackTrace();
        }
        return newObj;
    }

    public static JSONObject returnBook(Book anyBook){
        JSONObject newObj = new JSONObject();
        try {
            newObj.put("name", anyBook.name);
            newObj.put("author", anyBook.author);
            newObj.put("isbn", anyBook.isbn);
            newObj.put("taken", false);
            newObj.put("taken_by", "");
        } catch (JSONException e){
            e.printStackTrace();
        }
        return newObj;
    }

}

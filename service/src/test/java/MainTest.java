import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MainTest {

    @Test
    public void TestJSONOutput(){
        Book newBook = new Book("The Littles", "John Peterson", "9780590462259", false, "");
        User newUser = new User("Dasha", "Shum", 19);
        JSONObject takeBook = LibOperations.takeBook(newUser.firstName, newBook);
        assertEquals("{\"taken_by\":\"Dasha\",\"author\":\"John Peterson\",\"isbn\":\"9780590462259\",\"taken\":true,\"name\":\"The Littles\"}", takeBook.toString());
    }
}

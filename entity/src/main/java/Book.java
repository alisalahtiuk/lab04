public class Book {
    String name;
    String author;
    String isbn;
    boolean taken;

    String taken_by;

    public Book(String name, String author, String isbn, boolean taken, String taken_by) {
        this.name = name;
        this.author = author;
        this.isbn = isbn;
        this.taken = taken;
        this.taken_by = taken_by;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public String getIsbn() {
        return isbn;
    }

    public String getTaken_by() {
        return taken_by;
    }

    public boolean isTaken() {
        return taken;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setTaken(boolean taken) {
        this.taken = taken;
    }

    public void setTaken_by(String taken_by) {
        this.taken_by = taken_by;
    }
}
